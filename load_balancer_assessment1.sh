#!/bin/bash

if (( $#  > 0 ))
then
    hostname=$1
else
    echo "WTF: you must supply a hostname or IP address" 1>&2
    exit 1
fi 

sudo ssh -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem ubuntu@$hostname '
sudo apt-get -y install --no-install-recommends software-properties-common
sudo apt -y update
sudo add-apt-repository -y ppa:vbernat/haproxy-2.2
sudo apt-get -y install haproxy=2.2.\* -y
sudo update-rc.d -f haproxy enable

sudo mv -y -i /etc/haproxy/haproxy.cfg /etc/haproxy/haproxyold.cfg
cd /etc/haproxy/
sudo touch haproxy.cfg

sudo sh -c "cat >haproxy.cfg <<_END_
global
    log 127.0.0.1 local0 notice
    maxconn 2000
    user haproxy
    group haproxy

defaults
    log     global
    mode    http
    option  httplog
    option  dontlognull
    retries 3
    option redispatch
    timeout connect  5000
    timeout client  10000
    timeout server  10000

frontend simple_load_balancer
    bind *:80
    mode http
    option httpclose
    option forwardfor
    default_backend simple_servers

backend simple_servers
    balance roundrobin
    server lamp1 172.31.7.151:80
    server lamp2 172.31.14.103:80 
_END_"

haproxy -c -f /etc/haproxy/haproxy.cfg

'