#!/bin/bash

if (( $#  > 0 ))
then
    hostname=$1
else
    echo "WTF: you must supply a hostname or IP address" 1>&2
    exit 1
fi 


sudo scp -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem ubuntu.init ubuntu@$hostname:ubuntu

sudo ssh -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem ubuntu@$hostname '
sudo apt -y install nginx
sudo apt -y install rpm
if rpm -qa | grep "^nginx-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start nginx
else
    exit 1
fi

sudo mv ubuntu /etc/init.d/ubuntu
sudo chmod +x /etc/init.d/ubuntu
cd /etc/init.d/ubuntu
sudo update-rc.d aws enable
sudo nginx start

'