#!/bin/bash

if (( $#  > 0 ))
then
    hostname=$1
else
    echo "WTF: you must supply a hostname or IP address" 1>&2
    exit 1
fi 


sudo scp -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem aws.init ec2-user@$hostname:aws

sudo ssh -o StrictHostKeyChecking=no -i ~/.ssh/TanyaMollerKey.pem ec2-user@$hostname '
sudo yum -y install httpd
if rpm -qa | grep "^httpd-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start httpd
else
    exit 1
fi

sudo mv aws /etc/init.d/aws
sudo chmod +x /etc/init.d/aws
cd /etc/init.d/aws
sudo systemctl restart httpd
sudo systemctl enable aws
sudo /etc/init.d/aws start

'